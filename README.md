# Open Desk Booking

A simple tool to manage desks and rooms booking for a small team.

## Demo 

[POC - screenshoot video](https://framagit.org/open-desk-booking/tickets/uploads/9364e4ce03b6fdb679efcab802b8b628/Capture_vid%C3%A9o_du_2023-11-21_19-26-38.webm)